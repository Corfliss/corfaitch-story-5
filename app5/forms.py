from django import forms

class FormMatkul(forms.Form):
    nama_matkul = forms.CharField(label='Nama matkul', max_length=30)
    dosen = forms.CharField(label='Nama dosen', max_length=50)
    jumlah_sks = forms.IntegerField(label='Jumlah SKS')
    deskripsi = forms.CharField(label='Deskripsi', max_length=100)
    semester_tahun = forms.CharField(label='Semester/Tahun', max_length=30)
    ruang_kelas = forms.CharField(label='Ruang Kelas', max_length=50)

