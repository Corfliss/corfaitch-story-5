from django.db import models

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=50)
    jumlah_sks = models.IntegerField()
    deskripsi = models.CharField(max_length=100)
    semester_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=50)

# Create your models here.
