from django.urls import path
from . import views

app_name = 'app5'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('add_matkul', views.add_matkul, name='add_matkul'),
    path('delete/<int:delete_id>', views.delete, name='delete')
]