from django.shortcuts import render, redirect
from .models import Matkul
from .forms import FormMatkul

# Create your views here.
def index(request):
    response = {
        'form_matkul' : FormMatkul,
        'list_matkul' : Matkul.objects.all(),
    }

    return render(request, 'index.html', response)

def add_matkul(request):
    form = FormMatkul(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        add_matkul = Matkul.objects.create(nama_matkul=request.POST['nama_matkul'], dosen=request.POST['dosen'], 
        jumlah_sks=request.POST['jumlah_sks'], deskripsi=request.POST['deskripsi'], semester_tahun=request.POST['semester_tahun'], ruang_kelas=request.POST['ruang_kelas'])
    
    return redirect('app5:index')

def delete(request, delete_id):
   Matkul.objects.filter(id=delete_id).delete()
   return redirect('app5:index')
